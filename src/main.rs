use bevy::{
    prelude::*,
    app::AppExit,
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
};
use heron::prelude::*;

mod input;
mod camera;
mod map;
mod player;
mod off_map;
mod enemy;
mod projectile;

#[derive(PhysicsLayer)]
pub enum Layer {
    Map,
    Player,
    Projectile,
}

fn main() {
    App::new()
        .add_plugin(LogDiagnosticsPlugin::default())
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugins(DefaultPlugins)
        .add_plugin(PhysicsPlugin::default())
        .insert_resource(Gravity::from(Vec3::new(0.0, -9.81, 0.0)))
        .insert_resource(ClearColor(Color::rgb(0.7, 0.7, 1.0)))
        .add_startup_system(input::setup)
        .add_system(input::handle.label("input"))
        .add_startup_system(camera::setup)
        .add_system(camera::track_players)
        .add_startup_system(map::setup)
        .add_system(map::build)
        .add_system(map::collide)
        .add_system(player::spawn_player)
        .add_system(player::input.after("input"))
        .add_system(off_map::check)
        .add_system(enemy::walk)
        .add_system(projectile::collide)
        .add_system(exit_on_escape)
        // .add_system(log_collisions)
        .run();
}


fn exit_on_escape(keyboard_input: Res<Input<KeyCode>>, mut exit: EventWriter<AppExit>) {
    if keyboard_input.pressed(KeyCode::Escape) {
        exit.send(AppExit);
    }
}

#[allow(unused)]
fn log_collisions(mut events: EventReader<CollisionEvent>) {
    for event in events.iter() {
        match event {
            CollisionEvent::Started(d1, d2) => {
                println!("Collision started between {:?} and {:?}", d1, d2)
            }
            CollisionEvent::Stopped(d1, d2) => {
                println!("Collision stopped between {:?} and {:?}", d1, d2)
            }
        }
    }
}
