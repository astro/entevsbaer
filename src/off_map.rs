use bevy::prelude::*;

#[derive(Component)]
pub struct CanFallOffMap;

const MIN_Y: f32 = -20.0;

pub fn check(
    mut commands: Commands,
    entities: Query<(Entity, &Transform), With<CanFallOffMap>>,
) {
    for (entity, transform) in entities.iter() {
        if transform.translation.y < MIN_Y {
            commands.entity(entity).despawn_recursive();
        }
    }
}
