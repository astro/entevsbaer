use bevy::{
    prelude::*,
};
use heron::prelude::*;
use crate::Layer;

pub const EGG_RADIUS: f32 = 0.15;

#[derive(Component)]
pub struct Projectile;

pub fn collide(
    mut events: EventReader<CollisionEvent>,
    mut velocities: Query<&mut Velocity>
) {
    fn is_projectile(layers: CollisionLayers) -> bool {
        layers.contains_group(Layer::Projectile)
    }

    for event in events.iter() {
        let (entity_1, entity_2) = event.rigid_body_entities();
        let (layers_1, layers_2) = event.collision_layers();
        if is_projectile(layers_1) {
            let projectile_velocity = velocities.get(entity_1)
                .expect("entity_1");
            if let Ok(mut velocity) = velocities.get_mut(entity_2) {
                *velocity.linear = (Vec3::from(*velocity.linear) + Vec3::from(*projectile_velocity.linear)).into();
            }
        }
        if is_projectile(layers_2) {
            let projectile_velocity = velocities.get(entity_2)
                .expect("entity_2");
            if let Ok(mut velocity) = velocities.get_mut(entity_1) {
                *velocity.linear = (Vec3::from(*velocity.linear) + Vec3::from(*projectile_velocity.linear)).into();
            }
        }
    }
}
