{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
    fenix.url = "github:nix-community/fenix";
    fenix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, naersk, fenix }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
      rust = fenix.packages.${system}.stable.withComponents [
        "cargo"
        "rustc"
        "rust-src"  # just for rust-analyzer
        "clippy"
      ];

      # Override the version used in naersk
      naersk-lib = naersk.lib."${system}".override {
        cargo = rust;
        rustc = rust;
      };

      libpaths = with pkgs; lib.makeLibraryPath [
        vulkan-loader
        xlibs.libXcursor
        xlibs.libXi
        xlibs.libXrandr
      ];
    in rec {
      # `nix build`
      packages.entevsbaer = naersk-lib.buildPackage {
        pname = "entevsbaer";
        src = ./.;
        nativeBuildInputs = with pkgs; [
          makeWrapper
        ];
        buildInputs = with pkgs; [
          alsaLib
          cmake
          udev
          freetype
          expat
          openssl
          pkgconfig
          vulkan-validation-layers
          xlibs.libX11
        ];
        overrideMain = attrs: {
          installPhase = ''
            ${attrs.installPhase}

            wrapProgram $out/bin/entevsbaer \
              --prefix LD_LIBRARY_PATH : ${libpaths} \
              --set CARGO_MANIFEST_DIR $out/share/entevsbaer
            mkdir -p $out/share/entevsbaer
            cp -a assets $out/share/entevsbaer/
          '';
        };
        # doCheck = true;
        # cargoTestCommands = x:
        #   x ++ [
        #     # clippy
        #     ''cargo clippy --all --all-features --tests -- \
        #       -D clippy::pedantic \
        #       -D warnings \
        #       -A clippy::module-name-repetitions \
        #       -A clippy::too-many-lines \
        #       -A clippy::nonminimal_bool''
        #   ];
      };
      defaultPackage = packages.entevsbaer;

      checks = packages;

      # `nix run`
      apps.entevsbaer = utils.lib.mkApp {
        drv = packages.entevsbaer;
      };
      defaultApp = apps.entevsbaer;

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = [
          fenix.packages.${system}.rust-analyzer
        ] ++
        (with defaultPackage; nativeBuildInputs ++ buildInputs);
        shellHook = ''
          export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${libpaths}"
        '';
      };
    }) // {
      overlay = final: prev: {
        entevsbaer = self.packages.${prev.system};
      };
    };
}
