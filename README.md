# Usage

```bash
nix run git+https://gitea.c3d2.de/astro/entevsbaer
```

# Keybindings

Some USB gamepads work, some don't have recognizable buttons.

| Action | Left Keyboard Player | Right Keyboard Player |
|--------|----------------------|-----------------------|
| Walk   | W/A/S/D              | Arrow keys            |
| Jump   | Left Shift           | Right Shift           |
| Shoot  | Left Control         | Right Control         |
